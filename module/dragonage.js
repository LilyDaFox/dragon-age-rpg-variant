// Import Modules
import { DragonAgeActor } from "./actor/actor.js";
import { DragonAgeActorSheet } from "./actor/actor-sheet.js";
import { DragonAgeItem } from "./item/item.js";
import { DragonAgeItemSheet } from "./item/item-sheet.js";

Hooks.once('init', async function () {
    console.log("init called");
  game.dragonage = {
    DragonAgeActor,
    DragonAgeItem,
    rollItemMacro
  };
    console.log("game stuff done");


  /**
   * Set an initiative formula for the system
   * @type {String}
   */
  CONFIG.Combat.initiative = {
    formula: "3d10 + @abilities.dext.value",
    decimals: 2
  };

    console.log("config done ");

  // Define custom Entity classes
  CONFIG.Actor.entityClass = DragonAgeActor;
    CONFIG.Item.entityClass = DragonAgeItem;

    console.log("entities configed");

  // Register sheet application classes
   Actors.unregisterSheet("core", ActorSheet);
    console.log("core actor unregistered");
  Actors.registerSheet("dragonage", DragonAgeActorSheet, { makeDefault: true });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("dragonage", DragonAgeItemSheet, { makeDefault: true });

  // If you need to add Handlebars helpers, here are a few useful examples:
  Handlebars.registerHelper('concat', function() {
    var outStr = '';
    for (var arg in arguments) {
      if (typeof arguments[arg] != 'object') {
        outStr += arguments[arg];
      }
    }
    return outStr;
  });

  Handlebars.registerHelper('toLowerCase', function(str) {
    return str.toLowerCase();
  });

    Handlebars.registerHelper('toUpperCase', function(str) {
        return str.toUpperCase();
    });

    Handlebars.registerHelper("math", function (lvalue, operator, rvalue, options) {
        lvalue = parseFloat(lvalue);
        rvalue = parseFloat(rvalue);
        var operators, result;

        if (arguments.length < 3) {
            throw new Error("Handlerbars Helper 'math' needs 3 parameters");
        }

        if (options === undefined) {
            options = rvalue;
            rvalue = operator;
            operator = "+";
        }

        operators = {
            '+': function (l, r) { return l + r; },
            '-': function (l, r) { return l - r; },
            '*': function (l, r) { return l * r; },
            '/': function (l, r) { return l / r; },
            '%': function (l, r) { return l % r; }
        };

        if (!operators[operator]) {
            throw new Error("Handlerbars Helper 'math' doesn't know the operator " + operator);
        }

        result = operators[operator](lvalue, rvalue);
        return result;
    });

    Handlebars.registerHelper('compare', function (lvalue, operator, rvalue, options) {

        var operators, result;

        if (arguments.length < 3) {
            throw new Error("Handlerbars Helper 'compare' needs 2 parameters");
        }

        if (options === undefined) {
            options = rvalue;
            rvalue = operator;
            operator = "===";
        }

        operators = {
            '==': function (l, r) { return l == r; },
            '===': function (l, r) { return l === r; },
            '!=': function (l, r) { return l != r; },
            '!==': function (l, r) { return l !== r; },
            '<': function (l, r) { return l < r; },
            '>': function (l, r) { return l > r; },
            '<=': function (l, r) { return l <= r; },
            '>=': function (l, r) { return l >= r; },
            'typeof': function (l, r) { return typeof l == r; }
        };

        if (!operators[operator]) {
            throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);
        }

        result = operators[operator](lvalue, rvalue);

        if (result) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }

    });
});

Hooks.once("ready", async function() {
  // Wait to register hotbar drop hook on ready so that modules could register earlier if they want to
  Hooks.on("hotbarDrop", (bar, data, slot) => createDragonAgeMacro(data, slot));
});

/* -------------------------------------------- */
/*  Hotbar Macros                               */
/* -------------------------------------------- */

/**
 * Create a Macro from an Item drop.
 * Get an existing item macro if one exists, otherwise create a new one.
 * @param {Object} data     The dropped data
 * @param {number} slot     The hotbar slot to use
 * @returns {Promise}
 */
async function createDragonAgeMacro(data, slot) {
  if (data.type !== "Item") return;
  if (!("data" in data)) return ui.notifications.warn("You can only create macro buttons for owned Items");
  const item = data.data;

  // Create the macro command
  const command = `game.dragonage.rollItemMacro("${item.name}");`;
  let macro = game.macros.entities.find(m => (m.name === item.name) && (m.command === command));
  if (!macro) {
    macro = await Macro.create({
      name: item.name,
      type: "script",
      img: item.img,
      command: command,
      flags: { "dragonage.itemMacro": true }
    });
  }
  game.user.assignHotbarMacro(macro, slot);
  return false;
}

/**
 * Create a Macro from an Item drop.
 * Get an existing item macro if one exists, otherwise create a new one.
 * @param {string} itemName
 * @return {Promise}
 */
function rollItemMacro(itemName) {
  const speaker = ChatMessage.getSpeaker();
  let actor;
  if (speaker.token) actor = game.actors.tokens[speaker.token];
  if (!actor) actor = game.actors.get(speaker.actor);
  const item = actor ? actor.items.find(i => i.name === itemName) : null;
  if (!item) return ui.notifications.warn(`Your controlled Actor does not have an item named ${itemName}`);

  // Trigger the item roll
  return item.roll();
}