/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
import { AgeRoll } from "../ageRoll.js";

export class DragonAgeActorSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
        classes: ["dragonage", "sheet", "actor"],
        width: 800,
        height: 1000,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "abilities" }]
    });
  }

    /** @override */
  get template() {
        return `systems/dragonage/templates/actor/${this.actor.data.type}-sheet.html`;
  }

  /* -------------------------------------------- */

  

  /** @override */
  getData() {
    const data = super.getData();
    data.dtypes = ["String", "Number", "Boolean"];
    /*for (let attr of Object.values(data.data.attributes)) {
      attr.isCheckbox = attr.dtype === "Boolean";
     }*/
     
    // Prepare items.
      if (this.actor.data.type == 'character') {
          this._prepareCharacterItems(data);
      }

      else if (this.actor.data.type == 'npc') {
          this._prepareNPCItems(data);
      }

    return data;
  }

  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
  _prepareCharacterItems(sheetData) {
    const actorData = sheetData.actor;

      // Initialize containers.
      const weapons = [];
      const armor = [];
      const shields = [];
      const consumables = [];
      const loot = [];
      const talents = [];
      const features = [];
      const spells = [];
      const focuses = [];

    // Iterate through items, allocating to containers
    // let totalWeight = 0;
    for (let i of sheetData.items) {
      let item = i.data;
      i.img = i.img || DEFAULT_TOKEN;
        // Append to gear.
        switch (i.type) {
            case 'weapon':
                weapons.push(i);
                break;
            case 'armor':
                armor.push(i);
                break;
            case 'shield':
                shields.push(i);
                break;
            case 'consumable':
                consumables.push(i);
                break;
            case 'loot':
                loot.push(i);
                break;
            case 'feature':
                features.push(i);
                break;
            case 'talent':
                talents.push(i);
                break;
            case 'spell':
                spells.push(i);
                break;
            case 'focus':
                focuses.push(i);
            default:
                break;
        }


        
      
    }

      // Assign and return
      actorData.weapons = weapons;
      actorData.shields = shields;
      actorData.armor = armor;
      actorData.consumables = consumables;
      actorData.loot = loot;
      actorData.features = features;
      actorData.talents = talents;
      actorData.spells = spells;
      actorData.focuses = focuses;
  }

  _prepareNPCItems(sheetData) {
        const actorData = sheetData.actor;

        // Initialize containers.
        const weapons = [];
        const features = [];
        const spells = [];
        const focuses = [];

        // Iterate through items, allocating to containers
        // let totalWeight = 0;
        for (let i of sheetData.items) {
            let item = i.data;
            i.img = i.img || DEFAULT_TOKEN;
            // Append to gear.
            switch (i.type) {
                case 'weapon':
                    weapons.push(i);
                    break;
                case 'feature':
                    features.push(i);
                    break;
                case 'spell':
                    spells.push(i);
                    break;
                case 'focus':
                    focuses.push(i);
                    break;
                default:
                    break;
            }

        }

        // Assign and return
        actorData.weapons = weapons;
        actorData.features = features;
        actorData.spells = spells;
        actorData.focuses = focuses;
    }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Add Inventory Item
    html.find('.item-create').click(this._onItemCreate.bind(this));

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      item.sheet.render(true);
    });

      html.find('.item-edit').change(ev => {
          let id = $(ev.currentTarget).parents(".item").attr("data-item-id")
          let target = $(ev.currentTarget).attr("data-target")
          let item = duplicate(this.actor.getEmbeddedEntity("OwnedItem", id))
          setProperty(item, target, ev.target.value)
          this.actor.updateEmbeddedEntity(item)
      });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      this.actor.deleteOwnedItem(li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });

    // Rollable attributes.
      //html.find('.rollable').click(this._onRoll.bind(this));

      html.find(".rollable").click((ev) => {
          let target = $(ev.currentTarget).data();
          let ability = target.ability;
          let type = target.type;
          let item = target.item;
          let roll = target.roll;
          let rank = target.rank;
          if (ability == null) {
              ability = `dext`;
          }
          if (type == null) {
              type = `basic`;
          }
          if (rank == null) {
              rank = `Novice`;
          }
          if (type == `basic`) {
              let bRoll = new Roll(roll, this.actor.data.data);
              let label = target.label ? `${target.label}` : '';
              bRoll.roll().toMessage({
                  speaker: ChatMessage.getSpeaker({ actor: this.actor }),
                  flavor: label
              });
          } else {
              let ageRoll = new AgeRoll(this.actor, type, ability, item, roll, rank);
              ageRoll.render().then((content) => {
                  console.log(content);
                  ChatMessage.create({
                      user: game.user._id,
                      speaker: ChatMessage.getSpeaker({ actor: this.actor }),
                      content: content,
                  });
              });
          }
          
      });

    // Drag events for macros.
    if (this.actor.owner) {
      let handler = ev => this._onDragItemStart(ev);
      html.find('li.item').each((i, li) => {
        if (li.classList.contains("inventory-header")) return;
        li.setAttribute("draggable", true);
        li.addEventListener("dragstart", handler, false);
      });
    }
  }

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
  _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    // Get the type of item to create.
    const type = header.dataset.type;
    // Grab any data associated with this control.
    const data = duplicate(header.dataset);
    // Initialize a default name.
    const name = `New ${type.capitalize()}`;
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      data: data
    };
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.data["type"];

    // Finally, create the item!
    return this.actor.createOwnedItem(itemData);
  }


  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */
  _onRoll(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;

    if (dataset.roll) {
      let roll = new Roll(dataset.roll, this.actor.data.data);
      let label = dataset.label ? `Rolling ${dataset.label}` : '';
      roll.roll().toMessage({
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: label
      });
    }
  }


}
