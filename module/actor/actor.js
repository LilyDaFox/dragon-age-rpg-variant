/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class DragonAgeActor extends Actor {

  /**
   * Augment the basic actor data with additional dynamic data.
   */
    prepareData() {
        super.prepareData();

        const actorData = this.data;
        const data = actorData.data;
        const flags = actorData.flags;

        // Make separate methods for each Actor type (character, npc, etc.) to keep
        // things organized.

        //Prepare Base data
        if (actorData.type === 'character') this._prepareCharacterData(actorData);
        
    }
    
    _prepareCharacterData(actorData) {
        const data = actorData.data;

        // Make modifications to data here. For example:

   
    }

    getRollData() {
        const data = super.getRollData();
        // Map all items data using their slugified names
        data.items = this.data.items.reduce((obj, i) => {
            let key = i.name.slugify({ strict: true });
            let itemData = duplicate(i.data);
            obj[key] = itemData;
            return obj;
        }, {});
        return data;
    }

}