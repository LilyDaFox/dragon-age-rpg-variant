


export class AgeRoll extends Roll {
    constructor(target, type, ability, item, roll, rank) {
        const usedItem = target.items.find((i) => i.name == item);
        var focusBonus = 0;
        var situationBonus = 0;
        const abilityValue = target.data.data.abilities[ability].value;
        var modifier = 0;
        var tn = 10;
        var spellpower = 15;
        switch (type) {
            case `ability`:
                modifier = abilityValue;
                super(`3d10+${modifier}`);
                this.modifier = modifier;
                this.name = `${target.data.data.abilities[ability].label}`;
                this.rollType = type;
                break;
            case `focus`:
                focusBonus = AgeRoll.calculateFocus(usedItem);
                modifier = abilityValue + focusBonus;
                super(`3d10+${modifier}`);
                this.modifier = modifier;
                var a = String(ability).toUpperCase();
                this.name = `${item != undefined ? ` ${item}` : ""} (${a})`;
                this.rollType = type;
                break;
            case `weapon`:
                focusBonus = AgeRoll.calculateFocus(usedItem);
                modifier = abilityValue + focusBonus;
                super(`3d10+${modifier}`);
                this.modifier = modifier;
                this.name = `${item != undefined ? ` ${item}` : "Attack"}`;
                this.rollType = type;
                this.item = usedItem;
                break;
            case `spell`:
                focusBonus = AgeRoll.calculateFocus(usedItem);
                modifier = abilityValue + focusBonus;
                super(`3d10+${modifier}`);
                this.modifier = modifier;
                //this.spellpower = spellpower;
                //this.tn = tn;
                this.name = `${item != undefined ? ` ${item}` : "Spell"}`;
                this.rollType = type;
                this.item = usedItem;
                break;
            case `itemCard`:
                super('0');
                this.name = `${item != undefined ? ` ${item}` : ""}`;
                this.modifier = 0;
                this.rollType = type;
                this.item = usedItem;
                break;
            case 'talentCard':
                super('0');
                this.name = `${item != undefined ? ` ${item}` : "Talent"} (${usedItem.data.data.rank})`;
                this.modifier = 0;
                this.rollType = type;
                this.rank = rank;
                this.item = usedItem;
                break;
        }
    }

   
    static calculateFocus(focusItem) {
        if (focusItem != undefined) {
            if (focusItem.data.type == focus) {
                return focusItem.data.data.improved ? 3 : 2;
            }
            else {
                if (focusItem.data.data.improvedFocus) {
                    return 3;
                }
                else {
                    return focusItem.data.data.hasFocus ? 2 : 0;
                }
            }
        }
        else {
            return 0;
        }
    }
    async render(chatOptions = {}) {
        var template = `systems/dragonage/templates/chat/${this.rollType}-roll.html`;
        chatOptions = mergeObject({
            user: game.user._id,
            flavor: null,
            template: template,
        }, chatOptions || {});
        console.log(chatOptions.template);

        var sp = 0;
        var rolls;
        var isRoll = (this.rollType !== `itemCard` && this.rollType !== `talentCard`);
        if (isRoll) {
            if (!this._rolled) {
                this.roll();
            }
            rolls = this.dice[0].rolls.map((r) => r.roll);
            var pairs = 3 - new Set(rolls).size;
            if (pairs != 0) {
                sp = rolls[2];
                sp = pairs == 2 ? sp + 3 : sp;
            }
        }

        
        
        const chatData = {
            user: chatOptions.user,
            rolls: !isRoll ? null : rolls.reduce((p, c, i) => (Object.assign(Object.assign({}, p), { [`d${i}`]: c })), new Map()),
            name: this.name,
            modifier: this.modifier == 0
                ? undefined
                : this.modifier > 0
                    ? `+ ${this.modifier}`
                    : `- ${Math.abs(this.modifier)}`,
            total: this.total,
            stuntPoints: sp,
            item: this.item,
            rank: this.rank,
        };
        return renderTemplate(chatOptions.template, chatData);
    }
    async toMessage(chatData) {
        chatData.content = await this.render({ user: chatData.user });
        return ChatMessage.create(chatData);
    }
}
